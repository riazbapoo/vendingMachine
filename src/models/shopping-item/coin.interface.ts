export interface Coin {
    $key?: string;
    name: string,
    price: string,
    units: string,
}

