export interface ShoppingItem {
    $key?: string;
    image: string;
    price: number;
    quantity: number;
    description: string,
}