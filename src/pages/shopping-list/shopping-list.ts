import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, LoadingController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import { AddShoppingPage } from '../add-shopping/add-shopping';
import { ShoppingItem } from '../../models/shopping-item/shopping-item.interface';
import { Coin } from '../../models/shopping-item/coin.interface';
import { EditShoppingItemPage, DemoItem } from '../edit-shopping-item/edit-shopping-item';
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import firebase from 'firebase'

@Component({
  selector: 'page-shopping-list',
  templateUrl: 'shopping-list.html',
})
export class ShoppingListPage {

  shoppingListRef$: FirebaseListObservable<ShoppingItem[]>
  // demoListRef$: FirebaseListObservable<DemoItem[]>
  coinListRef$: FirebaseListObservable<Coin[]>

  demoItemSubscription: Subscription;
  // demoItemRef$: FirebaseObjectObservable<DemoItem>;
  // demoItem = {} as DemoItem;
  balance: any;
  result: any;
  loading: any;
  quantity: any
  coins: any[];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private db: AngularFireDatabase,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
) {

    this.shoppingListRef$ = this.db.list('vending');
    this.coinListRef$ = this.db.list('coin');
    // this.demoListRef$ = this.db.list('demo');
    // this.demoItemRef$ = this.db.object(`/demo/`);
    this.balance = 0.00
    this.result = 0.00


    // this.demoItemSubscription =
    //   this.demoItemRef$.subscribe(
    //     demoItem => this.demoItem = demoItem);
  }

  clearMoney() {
    this.balance = 0
    this.result = 0
  }

  addMoney(value) {
    this.balance = this.balance + value
    this.result = this.balance.toFixed(2)
    console.log(value + "  added, balance is: " + this.balance + " and result is " + this.result)
  }

  buyItem(shoppingItem: ShoppingItem) {

    if (shoppingItem.quantity == 0) {

      let alert = this.alertCtrl.create({
        title: 'Sorry this item is out of stock, please choose another item',
        buttons: [
          {
            text: 'Ok',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          }]
      });
      alert.present();
      return
    }

    if (this.result >= Number(shoppingItem.price)) {
      var change = this.result - Number(shoppingItem.price)
      this.balance = this.balance = this.balance - shoppingItem.price
      this.result = this.result - Number(shoppingItem.price)
      this.result = this.result.toFixed(2)
      console.log("case 1: difference is: " + change.toFixed(2))
      console.log("bought")



      if (change > 0) {
        let alert = this.alertCtrl.create({
          title: 'Dispensed: ' + shoppingItem.description + ' , You still have R ' + this.result + ' left',
          buttons: [
            {
              text: 'Buy More',
              role: 'cancel',
              handler: data => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Return Change',
              handler: data => {
                alert.dismiss().then(() => { this.clearMoney() })
                let alert2 = this.alertCtrl.create({
                  title: 'R' + this.result + ' change returned',
                  buttons: [
                    {
                      text: 'ok',
                      role: 'cancel'
                    }]
                })
                shoppingItem.quantity = Number(shoppingItem.quantity) - 1

                alert2.present()
                this.presentLoadingDefault()
              }
            }
          ]
        });
        alert.present();
        console.log("result is: " + this.result)
        this.getChange(this.result)
        console.log("you should get R" + change.toFixed(2) + " back")
        return
      }

      if (change == 0.00) {
        console.log(" exact amount. no change for you")
        let alert3 = this.alertCtrl.create({
          title: 'Dispensed: ' + shoppingItem.description + ", No change required. Thank you for purchasing",
          buttons: [
            {
              text: 'ok',
              role: 'cancel'
            }]
        })
        shoppingItem.quantity = Number(shoppingItem.quantity) - 1
        this.clearMoney()
        alert3.present()
        this.presentLoadingDefault()
        return
      }
      return
    }

    if (this.result < Number(shoppingItem.price)) {
      let alert4 = this.alertCtrl.create({
        title: "Please add more funds to purchase this product",
        buttons: [
          {
            text: 'ok',
            role: 'cancel'
          }]
      })
      alert4.present()
      return
    }
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Updating '
    })
    loading.present()

    setTimeout(() => {
      loading.dismiss()
    }, 500);
  }

  editShoppingItem(shoppingItem: ShoppingItem) {
    // Update our Firebase node with new item data
    // this.demoItemRef$.update(shoppingItem);

    // Send the user back to the ShoppingListPage
    //this.navCtrl.pop();
    alert("details updated successfully")
  }

  navigateToAddShoppingPage() {
    // Navigate the user to the AddShoppingPage
    this.navCtrl.push(AddShoppingPage);
  }

  getChange(difference) {

    let change = difference * 100
    let den = [5, 2, 1, 0.5];
    let denominations = den.map(x => x * 100);

    let final = []

    for (var i in denominations) {
      let item = denominations[i];
      final.push(Math.floor(change / item));
      change = Math.round(change % item);
    }

    console.log(final)

    for (var i in final) {
      let item = final[i];
      let alert2 = this.alertCtrl.create({
        title: item + ` x R` + den[i] + `'s`,
        buttons: [
          {
            text: 'ok',
            role: 'cancel'
          }]
      })
    }
    return final.reverse();
  }
}
